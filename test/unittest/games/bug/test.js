require = require('@std/esm')(module, { esm: 'mjs', cjs: true });

const OpenXum = require('../../../../lib/openxum-core').default;

describe( 'Engine', () =>{
  let engine = new OpenXum.Bug.Engine(OpenXum.Bug.GameType.STANDARD,OpenXum.Bug.Color.BLACK_STONE);
  test('Current player',() =>{
    expect(engine._current_color).toBe(OpenXum.Bug.Color.BLACK_STONE);
  });
});


describe( 'Board', () =>{
  test('Empty array',() =>{
  	let board = new OpenXum.Bug.Board();
    for (let row of board._array){
      for(color of row) {
		expect(color).not.toBe(OpenXum.Bug.Color.BLACK_STONE);
		expect(color).not.toBe(OpenXum.Bug.Color.WHITE_STONE);
	  }
    }
  });

  test('Place stone', () =>{
  	let board = new OpenXum.Bug.Board();
	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
	board.play_on(0,6,OpenXum.Bug.Color.BLACK_STONE);
	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
	expect(board._array[0][4]).toBe(OpenXum.Bug.Color.BLACK_STONE);
	expect(board._array[6][0]).toBe(OpenXum.Bug.Color.WHITE_STONE);
  });

  test('Is free', ()=>{
  	let board = new OpenXum.Bug.Board();
  	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(0,6,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
	expect(board.is_free(0,4)).toBeFalsy();
	expect(board.is_free(0,0)).toBeFalsy();
    expect(board.is_free(0,5)).toBeTruthy();
  });

  test('Player can play on', ()=>{
  	let board = new OpenXum.Bug.Board();
  	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(0,6,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
	expect(board.player_can_play_on(0,0,OpenXum.Bug.Color.BLACK_STONE)).toBeFalsy();
	expect(board.player_can_play_on(6,6,OpenXum.Bug.Color.BLACK_STONE)).toBeFalsy();

	expect(board.player_can_play_on(0,4,OpenXum.Bug.Color.BLACK_STONE)).toBeFalsy();
    expect(board.player_can_play_on(4,4,OpenXum.Bug.Color.BLACK_STONE)).toBeTruthy();
    board.set_stone(0,3,OpenXum.Bug.Color.BLACK_STONE);
	expect(board.player_can_play_on(0,5,OpenXum.Bug.Color.BLACK_STONE)).toBeFalsy();
	expect(board.player_can_play_on(1,5,OpenXum.Bug.Color.WHITE_STONE)).toBeTruthy();
	board.set_stone(1,5,OpenXum.Bug.Color.WHITE_STONE);
	expect(board.player_can_play_on(2,5,OpenXum.Bug.Color.WHITE_STONE)).toBeTruthy();
	board.set_stone(2,5,OpenXum.Bug.Color.WHITE_STONE);
	expect(board.player_can_play_on(3,5,OpenXum.Bug.Color.WHITE_STONE)).toBeFalsy();
  });

  test('Size biggest bug', () =>{
  	let board = new OpenXum.Bug.Board();
  	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
  	board.set_stone(0,5,OpenXum.Bug.Color.BLACK_STONE);
	expect(board._get_biggest_bug_size()).toBe(2);
  });

  test('Is merging', ()=>{
  	let board = new OpenXum.Bug.Board();
  	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(0,6,OpenXum.Bug.Color.BLACK_STONE);
  	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(0,3,OpenXum.Bug.Color.BLACK_STONE);
    board.set_stone(1,5,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(2,5,OpenXum.Bug.Color.WHITE_STONE);
	expect(board.is_merging(0,3,OpenXum.Bug.Color.BLACK_STONE)).toBeFalsy();
	expect(board.is_merging(0,5,OpenXum.Bug.Color.BLACK_STONE)).toBeTruthy();
	expect(board.is_merging(0,5,OpenXum.Bug.Color.WHITE_STONE)).toBeFalsy();
  });

  test('Remove bug', ()=>{
    let board = new OpenXum.Bug.Board();
    board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
    board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(0,3,OpenXum.Bug.Color.BLACK_STONE);
    board.set_stone(1,5,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(2,5,OpenXum.Bug.Color.WHITE_STONE);
  	board.remove_bug_on(0,3);
    expect(board.player_can_play_on(0,5,OpenXum.Bug.Color.BLACK_STONE)).toBeTruthy();
    expect(board.player_can_play_on(0,5,OpenXum.Bug.Color.WHITE_STONE)).toBeFalsy();
  });

  test('Equal board', ()=>{
    let board = new OpenXum.Bug.Board();
    board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
    board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(0,3,OpenXum.Bug.Color.BLACK_STONE);
    board.set_stone(1,5,OpenXum.Bug.Color.WHITE_STONE);
    board.set_stone(2,5,OpenXum.Bug.Color.WHITE_STONE);
    expect(board.is_equal_to(board)).toBeTruthy();
  });

  test('Clone board', ()=>{
	let board = new OpenXum.Bug.Board();
 	board.play_on(0,4,OpenXum.Bug.Color.BLACK_STONE);
 	board.play_on(6,0,OpenXum.Bug.Color.WHITE_STONE);
 	board.set_stone(0,3,OpenXum.Bug.Color.BLACK_STONE);
 	board.set_stone(1,5,OpenXum.Bug.Color.WHITE_STONE);
  	board.set_stone(2,5,OpenXum.Bug.Color.WHITE_STONE);
  	let board_clone = board.clone();
    expect(board_clone.is_equal_to(board)).toBeTruthy();
    board_clone.set_stone(5,3,OpenXum.Bug.Color.WHITE_STONE);
    expect(board_clone.is_equal_to(board)).toBeFalsy();
  });

  test('Can grow if eat', ()=>{
      let board2 = new OpenXum.Bug.Board();
      board2.set_stone(5,0,OpenXum.Bug.Color.WHITE_STONE);
      board2.set_stone(4,1,OpenXum.Bug.Color.WHITE_STONE);
      board2.set_stone(6,1,OpenXum.Bug.Color.WHITE_STONE);
      board2.set_stone(4,2,OpenXum.Bug.Color.BLACK_STONE);
      board2.set_stone(3,3,OpenXum.Bug.Color.BLACK_STONE);
      board2.set_stone(6,0,OpenXum.Bug.Color.BLACK_STONE);
      expect(board2.can_grow_if_eat(6,0,6,1)).toBeTruthy();
      board2.set_stone(5,2,OpenXum.Bug.Color.BLACK_STONE);
      expect(board2.can_grow_if_eat(6,0,6,1)).toBeFalsy();
  });

  test('Full game', ()=>{
    let game = new OpenXum.Bug.Board();
    while(game.player_can_play(OpenXum.Bug.Color.BLACK_STONE)) {
        for(let y=0;y<game._array.length;y++) {
            for (let x=0;x<game._array[0].length;x++) {
                if (game.player_can_play_on(x, y, OpenXum.Bug.Color.BLACK_STONE)) {
                    game.play_on(x, y, OpenXum.Bug.Color.BLACK_STONE);
                    let bugs_around = game._get_enemies_bugs_around(game._get_bug_on(x, y));
                    if(bugs_around.length>0) {
                        for (let bug of bugs_around) {
                            if (game._get_bug_on(x, y).can_eat(game._get_bug_on(bug._stones[0][0], bug._stones[0][1]))) {
                                game.remove_bug_on(bug._stones[0][0], bug._stones[0][1]);
                                let stones_playable = game.get_all_coordinates_where_grow(x, y);
                                if (stones_playable.length > 0)
                                    game.set_stone(game.get_all_coordinates_where_grow(x, y)[0][0], game.get_all_coordinates_where_grow(x, y)[0][1], OpenXum.Bug.Color.BLACK_STONE);
                                break;
                            }
                        }
                    }
                }
                x++;
                if (game.player_can_play_on(x, y, OpenXum.Bug.Color.WHITE_STONE)) {
                    game.play_on(x, y, OpenXum.Bug.Color.WHITE_STONE);
                    let bugs_around = game._get_enemies_bugs_around(game._get_bug_on(x, y))
                    if(bugs_around.length>0) {
                        for (let bug of bugs_around) {
                            if (game._get_bug_on(x, y).can_eat(game._get_bug_on(bug._stones[0][0], bug._stones[0][1]))) {
                                game.remove_bug_on(bug._stones[0][0], bug._stones[0][1]);
                                let stones_playable = game.get_all_coordinates_where_grow(x, y);
                                if (stones_playable.length > 0)
                                    game.set_stone(game.get_all_coordinates_where_grow(x, y)[0][0], game.get_all_coordinates_where_grow(x, y)[0][1], OpenXum.Bug.Color.WHITE_STONE);
                                break;
                            }
                        }
                    }
                }
            }
        }
    }
  });
});

describe( 'Bug', () =>{
	let b1 = new OpenXum.Bug.Bug(0,2,OpenXum.Bug.Color.BLACK_STONE);
	let b2 = new OpenXum.Bug.Bug(0,3,OpenXum.Bug.Color.WHITE_STONE);
	test('Can eat' , ()=>{
		expect(b1.can_eat(b2)).toBeTruthy();
		expect(b2.can_eat(b1)).toBeTruthy();
	});
	test('Add stone', ()=>{
		b1.add(1,2);
		b2.add(1,3);
		expect(b1.get_size()).toBe(2);
		expect(b2.get_size()).toBe(2);
	});
	test('Is same shape', ()=>{
		let shape1 = new OpenXum.Bug.Bug(3,4,OpenXum.Bug.Color.BLACK_STONE);
		let shape2 = new OpenXum.Bug.Bug(1,4,OpenXum.Bug.Color.WHITE_STONE);
		shape1.add(3,3);
        shape1.add(2,5);
        shape1.add(5,4);
        shape1.add(4,4);
        shape1.add(1,5);
        shape1.add(1,4);
        shape1.add(2,3);
        shape1.add(3,5);

        shape2.add(2,4);
        shape2.add(2,5);
        shape2.add(3,3);
        shape2.add(4,2);
        shape2.add(1,6);
        shape2.add(0,6);
        shape2.add(0,5);
        shape2.add(2,3);
		expect(shape1.is_same_shape(shape2)).toBeTruthy();

	});

    test('Encode', ()=>{
        let bug = new OpenXum.Bug.Bug(2,2, OpenXum.Bug.Color.BLACK_STONE);
        bug.add(2,3);
        bug.add(2,4);
        bug.add(3,3);
        expect(bug.encode()).toBe("22232433B");
    });

    test('Decode', ()=>{
        let bug = new OpenXum.Bug.Bug();
        bug.decode("010203W");
        expect(OpenXum.Bug.Bug.compare_vectors(bug._stones,[[0,1],[0,2],[0,3]])).toBeTruthy();
        expect(bug.get_color()).toBe(OpenXum.Bug.Color.WHITE_STONE);
    });

	test('rotate',()=>{
		let bug = new OpenXum.Bug.Bug(2,2, OpenXum.Bug.Color.BLACK_STONE);
		bug.add(2,3);
		bug.add(2,4);
		bug.add(3,3);
        let rotated_bug = bug.rotate();
        expect(OpenXum.Bug.Bug.compare_vectors(rotated_bug._stones,[[2,2],[3,2],[4,2],[4,1]])).toBeTruthy();
        rotated_bug = rotated_bug.rotate();
        expect(OpenXum.Bug.Bug.compare_vectors(rotated_bug._stones,[[2,2],[3,1],[4,0],[3,0]])).toBeTruthy();
        expect(OpenXum.Bug.Bug.compare_vectors(rotated_bug._stones,[[2,2],[3,1],[4,0],[2,0]])).toBeFalsy();
	});

	test('mirror',()=> {
        let bug = new OpenXum.Bug.Bug(2,2, OpenXum.Bug.Color.BLACK_STONE);
        bug.add(2,3);
        bug.add(2,4);
        bug.add(3,3);

        let mirror_bug = bug.mirror();
        expect(OpenXum.Bug.Bug.compare_vectors(mirror_bug._stones,[[2,2],[2,3],[2,4],[1,4]])).toBeTruthy();
        mirror_bug = mirror_bug.mirror();
        expect(mirror_bug.is_equal_to(bug)).toBeTruthy();
	});

	test('Equal bug', ()=>{
		expect(b1.is_equal_to(b1)).toBeTruthy();
		expect(b1.is_equal_to(b2)).toBeFalsy();
	});

	test('Clone bug', ()=>{
		let bug_clone = b1.clone();
		expect(bug_clone.is_equal_to(b1)).toBeTruthy();
		bug_clone.add(3,3);
        expect(bug_clone.is_equal_to(b1)).toBeFalsy();
	});

	test('compare vectors', ()=>{
		let vector1 =[[0,1],[0,2],[0,3]];
		let vector2 = [[0,3],[0,1],[0,2]];
		expect(OpenXum.Bug.Bug.compare_vectors(vector1,vector2)).toBeTruthy();
	});
});
