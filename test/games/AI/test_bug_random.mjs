require = require('@std/esm')(module, {esm: 'mjs', cjs: true});

const OpenXum = require('../../../lib/openxum-core/').default;
const AI = require('../../../lib/openxum-ai/').default;

let win_white=0;
let win_black=0;
for (let game = 0; game < 20; game++) {
  let e = new OpenXum.Bug.Engine(OpenXum.Bug.GameType.STANDARD, OpenXum.Bug.Color.BLACK_STONE);
  //let p1 = new AI.Generic.RandomPlayer(OpenXum.Bug.Color.BLACK_STONE, OpenXum.Bug.Color.WHITE_STONE, e);
  let p1 = new AI.Specific.Bug.AlphaBetaPlayer(OpenXum.Bug.Color.BLACK_STONE, OpenXum.Bug.Color.WHITE_STONE, e);
  let p2 = new AI.Generic.RandomPlayer(OpenXum.Bug.Color.WHITE_STONE, OpenXum.Bug.Color.BLACK_STONE, e);

  //p2 = new AI.Specific.Bug.MCTSPlayer(OpenXum.Bug.Color.WHITE_STONE, OpenXum.Bug.Color.BLACK_STONE, e);
  let p = p1;
  let moves = [];
  let turn = 0;
  while (!e.is_finished()) {
    const move = p.move();
    moves.push(move);
    e.move(move);
    p = e.current_color() === p1.color() ? p1 : p2;
    turn++;
  }
  console.log("Winner is " + (e.winner_is() === OpenXum.Bug.Color.BLACK_STONE ? "BLACK" : "WHITE"));
  if (e.winner_is() === OpenXum.Bug.Color.BLACK_STONE) win_black++;
  else win_white++;
  for (let index = 0; index < moves.length; ++index) {
    //console.log(moves[index].to_string());
  }
  console.log(turn);
}
console.log("Black wins: "+win_black );
console.log("White wins: "+win_white );