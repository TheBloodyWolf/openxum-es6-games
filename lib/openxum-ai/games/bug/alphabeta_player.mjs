"use strict";

import AlphaBeta from '../../generic/alphabeta_player.mjs';
import Phase from '../../../openxum-core/games/bug/phase.mjs';


class AlphaBetaPlayer extends AlphaBeta {
  constructor(c, o, e) {
    super(c, o, e, 5, 1000);
  }

  evaluate(e, depth) {
    if (e.is_finished()) {
      switch (e.winner_is()) {
        case this.color():
          return this.VICTORY_SCORE;
        case this.opponent_color():
          return -this.VICTORY_SCORE;
        default:
          return 0;
      }
    }
    let score = 0;


    let max = 0, max_opponent = 0;
    for(let bug of e.get_board().get_bugs_by_color(this.color())){
      max = max<bug.get_size()?bug.get_size():max;
    }
    for(let bug of e.get_board().get_bugs_by_color(this.opponent_color())){
      max_opponent = max_opponent<bug.get_size()?bug.get_size():max_opponent;
    }
    score+=max/2;

    let defensive=0,offensive = 0;
    if(max<max_opponent) {
      defensive = 5;
      offensive = 60;
    }
    else {
      defensive = 60;
      offensive = 5;
    }
    score+=this._eat_score_value(e,offensive,defensive);
    score += this._put_stone_value(e,offensive,defensive);

    return score;
  }

  _eat_score_value(e,offensive,defensive){
    let score = 0;
    if (e.phase() === Phase.EAT && e.current_color() === this.color()) {
      if (e.get_possible_move_list().length > 0) score += offensive;
    }

    else if (e.phase() === Phase.EAT && e.current_color() === this.opponent_color()){
      if (e.get_possible_move_list().length === 0) score += defensive;
    }
    return score;
  }

  _put_stone_value(e,offensive,defensive){
    let score = 0;
    if (e.phase() === Phase.PUT_STONE && e.current_color() === this.color()) {
      score -=e.get_possible_move_list().length/defensive;
    }
    else if (e.phase() === Phase.PUT_STONE && e.current_color() === this.opponent_color()) {
      score += e.get_possible_move_list().length*(offensive/10);
    }
    return score;
  }
}


export default AlphaBetaPlayer;