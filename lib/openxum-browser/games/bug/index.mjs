"use strict";

import Bug from '../../../openxum-core/games/bug/index.mjs';
import AI from '../../../openxum-ai/generic/index.mjs';
import Alb from '../../../openxum-ai/games/bug/alphabeta_player.mjs'
import Gui from './gui.mjs';
import Manager from './manager.mjs';

export default {
  Gui: Gui,
  Manager: Manager,
  Settings: {
    ai: {
      mcts: Alb
    },
    colors: {
      first: Bug.Color.BLACK_STONE,
      init: Bug.Color.BLACK_STONE,
      list: [
        {key: Bug.Color.BLACK_STONE, value: 'black'},
        {key: Bug.Color.WHITE_STONE, value: 'white'}
      ]
    },
    modes: {
      init: Bug.GameType.REGULAR,
      list: [
        {key: Bug.GameType.BLITZ, value: 'blitz'},
        {key: Bug.GameType.REGULAR, value: 'regular'}
      ]
    },
    opponent_color(color) {
      return color === Bug.Color.BLACK_STONE ? Bug.Color.WHITE_STONE : Bug.Color.BLACK_STONE;
    },
    types: {
      init: 'ai',
      list: [
        {key: 'gui', value: 'GUI'},
        {key: 'ai', value: 'AI'},
        {key: 'online', value: 'Online'},
        {key: 'offline', value: 'Offline'}
      ]
    }
  }
};




