"use strict";

import OpenXum from '../../openxum/manager.mjs';
import Bug from '../../../openxum-core/games/bug/index.mjs';


class Manager extends OpenXum.Manager {
  constructor(t, e, g, o, s, w, f) {
    super(t, e, g, o, s, w, f);
  }

  build_move() {
    return new Bug.Move();
  }

  get_current_color() {
    return this._engine.current_color() === Bug.Color.WHITE_STONE ? 'White' : 'Black';
  }

  static get_name() {
    return 'bug';
  }

  get_winner_color() {
    return this.engine().winner_is() === Bug.Color.WHITE_STONE ? 'white' : 'black';
  }

  process_move() { }
}

export default Manager;

