// lib/openxum-browser/games/lyngk/gui.mjs
import Graphics from '../../graphics/index.mjs';
import OpenXum from '../../openxum/gui.mjs';
import Bug from "../../../openxum-core/games/bug/index.mjs";
import Phase from "../../../openxum-core/games/bug/phase.mjs";
import Move from "../../../openxum-core/games/bug/move.mjs";
import MoveType from "../../../openxum-core/games/bug/move_type.mjs";
//import Color from "../../../openxum-core/games/bug/color";

class Gui extends OpenXum.Gui {
  constructor(c, e, l, g) {
    super(c, e, l, g);

    this._hexagon_size = 0;
    this._cursor_x = 0;
    this._cursor_y = 0;
  }

  _get_pixels_by_coordinates(x,y){
    return {x: (this._canvas.height/14 + 30)*(x+1), y:this._hexagon_size/2*(x-2)+this._hexagon_size*y};
  }

  _get_coordinates_by_pixels(x,y){
    for(let i=0;i<7;i++){
      for(let j=0;j<7;j++){
        if(this._is_in_range_by_coordinates(x,y,i,j,this._hexagon_size/2))
          return {x: i, y: j};
      }
    }
    return false;
  }

  _is_in_range_by_coordinates(x,y,coordinate_x,coordinate_y,radius){
    let coordinates = this._get_pixels_by_coordinates(coordinate_x,coordinate_y);
    return Math.abs(x-coordinates.x)<=radius && Math.abs(y-coordinates.y)<=radius;
  }

  draw() {
    this._context.lineWidth = 1;

    this._context.strokeStyle = "#000000";
    this._context.fillStyle = "#ffffff";
    Graphics.board.draw_round_rect(this._context, 0, 0, this._canvas.width, this._canvas.height, 17, true, true);

    this._draw_grid();
    this._draw_possible_moves();
  }

  _draw_hexagon_by_coordinates(x,y,color){
    let coordinates = this._get_pixels_by_coordinates(x,y);
    this._draw_hexagon(coordinates.y,coordinates.x,color);
  }

  _draw_hexagon(x_center, y_center, color) {
    this._setup_hexagon_style(x_center, y_center, color);
    let size_stone = this._hexagon_size/2;


    this._context.moveTo(x_center, y_center + size_stone);

    this._context.beginPath();
    for (let angle = 0.0; angle < 2 * Math.PI; angle += Math.PI / 3) {
      this._context.lineTo(size_stone * Math.sin(angle) + x_center, size_stone * Math.cos(angle) + y_center);
    }
    this._context.closePath();
    this._context.fill();

    this._draw_center_hexagon(x_center, y_center, color);
  }

  _draw_center_hexagon(x_center, y_center, color){

    if (color === Bug.Color.BLACK_STONE) {
      this._context.fillStyle ='#292e30';
    }
    else if(color === Bug.Color.WHITE_STONE) {
      this._context.fillStyle ='#cbd4d8';
    }

    let size_stone_in = this._hexagon_size/3;

    this._context.moveTo(x_center, y_center + size_stone_in);

    this._context.beginPath();
    for (let angle = 0.0; angle < 2 * Math.PI; angle += Math.PI / 3) {
      this._context.lineTo(size_stone_in * Math.sin(angle) + x_center, size_stone_in * Math.cos(angle) + y_center);
    }
    this._context.closePath();
    this._context.fill();
  }

  _setup_hexagon_style(x_center, y_center, color){
    if (color === Bug.Color.EMPTY_STONE) {
      this._context.fillStyle = "#ffce86";
    }
    else if (color === Bug.Color.BLACK_STONE) {
      let lingrad = this._context.createLinearGradient(x_center-20,y_center-20,x_center+20,y_center+20);
      lingrad.addColorStop(0, '#000000');
      lingrad.addColorStop(0.8, '#505557');
      lingrad.addColorStop(1, '#6a6e71');
      this._context.fillStyle = lingrad;
    }
    else if(color === Bug.Color.WHITE_STONE) {
      let lingrad = this._context.createLinearGradient(x_center-20,y_center-20,x_center+20,y_center+20);
      lingrad.addColorStop(0, '#f4f4f4');
      lingrad.addColorStop(0.8, '#adb6ba');
      lingrad.addColorStop(1, '#939393');
      this._context.fillStyle = lingrad;
    }
    else {
      this._context.fillStyle = "#ffffff";
    }
    this._context.strokeStyle = "#ffffff";

  }

  _draw_highlight_hexagon_on_coordinates(coordinate_x,coordinate_y){
    let coordinates = this._get_pixels_by_coordinates(coordinate_x,coordinate_y);
    this._draw_highlight_hexagon(coordinates.y,coordinates.x);
  }

  _draw_highlight_hexagon(x,y){
    let size_stone = this._hexagon_size/2;

    this._context.strokeStyle = "#8aa5ff";

    this._context.moveTo(x, y + size_stone);

    this._context.lineWidth = 5;

    for (let angle = 0.0; angle < 2 * Math.PI; angle += Math.PI / 3) {
      this._context.lineTo(size_stone * Math.sin(angle) + x, size_stone * Math.cos(angle) + y);
    }
    this._context.closePath();
    this._context.stroke();
  }

  _draw_grid() {
    for(let y=0;y<7;y++){
      for(let x=0;x<7;x++){
        this._draw_hexagon_by_coordinates(x,y,this._engine.get_board().get_stone(x,y));
      }
    }
  }

  get_move() {
    if (this._engine.phase() === Phase.PUT_STONE) {
      return new Move(MoveType.PUT_STONE,this._engine.current_color(),this._cursor_x,this._cursor_y,null,null);
    }
    if (this._engine.phase() === Phase.EAT) {
      return new Move(MoveType.EAT,this._engine.current_color(),null,null,this._engine._eating_bug,this._engine.get_board()._get_bug_on(this._cursor_x,this._cursor_y));
    }
    if (this._engine.phase() === Phase.GROW) {
      return new Move(MoveType.GROW,this._engine.current_color(),this._cursor_x,this._cursor_y,null,null);
    }
  }

  is_animate() {return false;}

  is_remote() {return false;}

  move(move, color) {this._manager.play();}

  unselect() {}

  set_canvas(c) {
    super.set_canvas(c);

    this._hexagon_size = this._canvas.width/7;

    this._canvas.addEventListener("click", (e) => {
      this._on_click(e);
    });
    this._canvas.addEventListener("mousemove", (e) => {
      this._on_move(e);
    });

    this.draw();
  }

  _draw_possible_moves() {
    let list = this._engine.get_possible_move_list();
    if(this._engine.phase()=== Phase.EAT){
      for(let move of list){
        for(let stone of move._eated_bug.get_stones())
          this._draw_highlight_hexagon_on_coordinates(stone[0], stone[1]);
      }
    }
    else {
      for (let move of list) {
        this._draw_highlight_hexagon_on_coordinates(move._x, move._y);
      }
    }
  }

  _on_move(event) {}

  _get_click_position(e) {
    let rect = this._canvas.getBoundingClientRect();
    return {x: (e.clientX - rect.left) * this._scaleX, y: (e.clientY - rect.top) * this._scaleY};
  }

  _on_click(event) {
    let pos = this._get_click_position(event);
    let coordinates = this._get_coordinates_by_pixels(pos.y,pos.x);
    this._cursor_x = coordinates.x;
    this._cursor_y = coordinates.y;

    if(this._engine.move_is_valid(this.get_move()))
      this._manager.play();
  }

}

export default Gui;


