"use strict";

const Phase = {PUT_STONE: 0, EAT: 1, GROW: 2};

export default Phase;
