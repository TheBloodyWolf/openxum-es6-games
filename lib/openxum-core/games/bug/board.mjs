"use strict";

import Color from './color.mjs';
import Bug from './bug.mjs';

const SIZE_BOARD = 7;

class Board {

  constructor() {
    this._array = [];
    this._bugs = [];

    for(let y=0; y<SIZE_BOARD; y++) {
      this._array.push([Color.EMPTY_STONE]);
      for (let x = 0; x < SIZE_BOARD-1; x++) {
        this._array[y].push(Color.EMPTY_STONE);
      }
    }
    this._setup_disabled_corners();
  }


  clone(){
    let board_clone = new Board();
    for(let y=0;y<SIZE_BOARD;y++){
      for(let x=0;x<SIZE_BOARD;x++) {
        board_clone.get_array()[x][y] = this.get_stone(x,y);
      }
    }
    for(let bug of this._bugs){
      board_clone.get_bugs().push(bug.clone());
    }
    return board_clone;
  }

  get_stone(x,y){
    if(Board._check_correct_coordinate(x,y))
      return this._array[x][y];
    return Color.DISABLED;
  }

  get_array(){
    return this._array;
  }

  get_bugs(){
    return this._bugs;
  }

  set_stone(x, y, color){
    if(Board._check_correct_coordinate(x,y)) {
      this._array[x][y] = color;
      if(color!==Color.EMPTY_STONE && color!==Color.DISABLED) {
        let bugs_around = this._get_surround_bugs_by_color(x, y, color);
        if (bugs_around.length === 1) {
          this._get_surround_bugs_by_color(x, y, color)[0].add(x, y);
        }
        else if (bugs_around.length === 0) {
          this._bugs.push(new Bug(x, y, color));
        }
      }
    }
  }

  toString(){
    let string = '';
    for(let row of this._array){
      for(let stone of row){
        string+=stone.toString();
        string+="  ";
      }
      string+='\n';
    }
    string+='\n';
    for(let bug of this._bugs){
      string+=bug.toString();
      string+='\n';
    }
    return string;
  }

  get_bugs_by_color(color){
    let bugs_list = [];
    for(let bug of this._bugs)
      if(bug.get_color() === color) bugs_list.push(bug);
    return bugs_list;

  }

  static _check_if_bug_already_in_array(bugs, x, y){
    let is_already_in_bugs = false;
    for(let bug of bugs){
      if(bug.is_on(x,y))
        is_already_in_bugs = true;
    }
    return is_already_in_bugs;
  }

  static _get_surround_coordinate(x,y){
    let coordinates = [];
    coordinates.push([x-1,y]);
    coordinates.push([x-1,y+1]);
    coordinates.push([x,y+1]);
    coordinates.push([x+1,y]);
    coordinates.push([x+1,y-1]);
    coordinates.push([x,y-1]);
    return coordinates;
  }

  static _check_correct_coordinate(x, y){
    return !(x < 0 || x >= SIZE_BOARD || y < 0 || y >= SIZE_BOARD);
  }



  get_all_coordinates_where_grow_from_bug(bug){
    let coordinates_around = bug.get_all_coordinates_around();
    let valid_coordinates = [];
    for(let coordinate of coordinates_around){
      if(this.bug_can_grow_on(coordinate[0],coordinate[1],bug.get_color()))
        valid_coordinates.push(coordinate);
    }
    return valid_coordinates;
  }

  get_all_coordinates_where_grow(x,y){
    return this.get_all_coordinates_where_grow_from_bug(this._get_bug_on(x,y));
  }

  growing_bug_size(x,y, color){
    let bug_next_to = this._get_surround_bugs_by_color(x,y,color);
    if(bug_next_to.length === 1)
      return bug_next_to[0].get_size();
    return 0;
  }



  bug_can_grow_on(x,y,color){
    return Board._check_correct_coordinate(x, y) && this.is_free(x, y) && !this.is_merging(x, y, color);
  }

  can_grow_if_eat(x_eater,y_eater,x_eaten,y_eaten){
    let bug_eater = this._get_bug_on(x_eater,y_eater);
    let bug_eaten = this._get_bug_on(x_eaten,y_eaten);
    return this.can_grow_if_eat_bug(bug_eater, bug_eaten);
  }

  can_grow_if_eat_bug(bug_eater, bug_eaten){
    let board_clone = this.clone();
    let bug_eater_clone = board_clone._get_bug_on(bug_eater.get_stones()[0][0],bug_eater.get_stones()[0][1]);
    let bug_eaten_clone = board_clone._get_bug_on(bug_eaten.get_stones()[0][0],bug_eaten.get_stones()[0][1]);

    board_clone.remove_bug(bug_eaten_clone);
    let bug_eater_surround_coordinates = bug_eater_clone.get_all_coordinates_around();
    for(let coordinate of bug_eater_surround_coordinates){
      if(board_clone.bug_can_grow_on(coordinate[0],coordinate[1],bug_eater_clone.get_color()))
        return true;
    }
    return false;
  }

  is_disabled(x, y){
    return Board._check_correct_coordinate(x,y) && this._array[x][y]===Color.DISABLED;
  }

  is_equal_to(board){
    if(board.get_array().length !== this._array.length || board.get_bugs().length !== this._bugs.length)
      return false;
    for(let y=0;y<SIZE_BOARD;y++){
      for(let x=0;x<SIZE_BOARD;x++) {
        if(this.get_stone(x,y)!==board.get_stone(x,y))
          return false;
      }
    }
    for(let bug=0;bug<this._bugs.length;bug++){
      if(!this._bugs[bug].is_equal_to(board.get_bugs()[bug]))
        return false;
    }
    return true;
  }

  is_free(x,y){
    return this._array[x][y]===Color.EMPTY_STONE;
  }

  is_merging(x,y,color){
    let bugs_same_color_around = this._get_surround_bugs_by_color(x,y,color);
    return bugs_same_color_around.length > 1;
  }

  player_can_play(color){
    for(let y=0;y<SIZE_BOARD;y++){
      for(let x=0;x<SIZE_BOARD;x++){
        if(this.player_can_play_on(x,y,color))
          return true;
      }
    }
    return false;
  }

  player_can_play_on(x,y,color){
    return this.bug_can_grow_on(x,y,color) &&
      (this.growing_bug_size(x, y, color) + 1 <= this._get_biggest_bug_size() ||
      this._get_biggest_bug_size() === 0);
  }



  play_on(x, y, color) {
    if (this.player_can_play_on(x, y, color)) {
      this.set_stone(x, y, color);
    }
  }

  remove_bug(bug){
      let index = this._bugs.indexOf(bug);
      if(index > -1) {
          for(let stone of this._bugs[index].get_stones()){
              this._array[stone[0]][stone[1]]=Color.EMPTY_STONE;
          }
          this._bugs.splice(index,1);
      }
  }

  remove_bug_on(x,y){
    if(Board._check_correct_coordinate(x,y)){
      this.remove_bug(this._get_bug_on(x,y));
    }
  }


    
  _get_bug_on(x,y){
    for(let bug of this._bugs){
      if(bug.is_on(x,y))
        return bug;
    }
    return new Bug(-1,-1,Color.DISABLED);
  }

  _get_enemies_bugs_around(bug){
    let bugs_around = [];
    let arroud_coordinates,bug_already_in;
    arroud_coordinates = bug.get_all_coordinates_around();
    for(let coords of arroud_coordinates) {
      bug_already_in = false;
      if (this.get_stone(coords[0], coords[1]) !== bug.get_color()
        && this.get_stone(coords[0], coords[1]) !== Color.DISABLED
        && this.get_stone(coords[0], coords[1]) !== Color.EMPTY_STONE) {
        for (let bug_around of bugs_around) {
          if (bug_around.contain(coords[0], coords[1])){
            bug_already_in = true;
            break;
          }
        }
        if (!bug_already_in) bugs_around.push(this._get_bug_on(coords[0], coords[1]));
      }
    }
    return bugs_around;
  }

  _get_biggest_bug_size(){
    if(this._bugs.length === 0)
      return 0;
    let max=0;
    for(let bug of this._bugs){
      if(max < bug.get_size())
        max = bug.get_size();
    }
    return max;
  }

  _get_surround_bugs(x,y) {
    let bugs = [];
    let surround_coordinates = Board._get_surround_coordinate(x, y);
    for (let coordinate of surround_coordinates) {
      if (Board._check_correct_coordinate(coordinate[0],coordinate[1]) &&
          !this.is_free(coordinate[0], coordinate[1]) &&
          !Board._check_if_bug_already_in_array(bugs, coordinate[0], coordinate[1]) &&
          !this.is_disabled(coordinate[0], coordinate[1]))
            bugs.push(this._get_bug_on(coordinate[0], coordinate[1]));
    }
    return bugs;
  }

  _get_surround_bugs_by_color(x,y, color) {
    let bugs = this._get_surround_bugs(x,y);
    if (bugs.length > 0) {
      let sort_bugs = [];
      for (let bug of bugs) {
        if (bug.get_color() === color)
          sort_bugs.push(bug);
      }
      return sort_bugs;
    }
    return [];
  }

  _setup_disabled_corners(){
    this._array[0][0]=Color.DISABLED;
    this._array[0][1]=Color.DISABLED;
    this._array[0][2]=Color.DISABLED;
    this._array[1][1]=Color.DISABLED;
    this._array[1][0]=Color.DISABLED;
    this._array[2][0]=Color.DISABLED;

    this._array[6][6]=Color.DISABLED;
    this._array[6][5]=Color.DISABLED;
    this._array[5][6]=Color.DISABLED;
    this._array[5][5]=Color.DISABLED;
    this._array[4][6]=Color.DISABLED;
    this._array[6][4]=Color.DISABLED;
  }

}

export default Board;
