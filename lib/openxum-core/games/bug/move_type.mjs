"use strict";

const MoveType = {PUT_STONE: 0, GROW: 1, EAT: 2};

export default MoveType;
