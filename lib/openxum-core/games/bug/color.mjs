"use strict";

const Color = {DISABLED: -1, EMPTY_STONE:0, BLACK_STONE: 1, WHITE_STONE: 2};

export default Color;
