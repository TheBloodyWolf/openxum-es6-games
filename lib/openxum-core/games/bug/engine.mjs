// lib/openxum-core/games/bug/engine.mjs

import OpenXum from '../../openxum/index.mjs';
import Board from './board.mjs';
import Move from "./move.mjs";
import Phase from './phase.mjs';
import MoveType from './move_type.mjs';
import Color from './color.mjs';

class Engine extends OpenXum.Engine {
  constructor(game_type, color) {
    super();
    this._type = game_type;
    this._current_color = color;
    this._phase = Phase.PUT_STONE;
    this._x = null;
    this._board = new Board();
    this._eating_bug = null;
  }

  build_move() {
    return new Move();
  }

  clone() {
    let clone_engine = new Engine(this.type, this._current_color);
    clone_engine._type = this._type;
    clone_engine._current_color = this._current_color;
    clone_engine._phase = this._phase;
    if (this._eating_bug !== null)
      clone_engine._eating_bug = this._eating_bug.clone();
    else
      clone_engine._eating_bug = null;
    clone_engine._board = this._board.clone();
    return clone_engine;
  }

  current_color() {
    return this._current_color;
  }

  get_name() {
    return 'Bug';
  }

  get_board() {
    return this._board;
  }

  phase(){
    return this._phase;
  }

  put_stone_move_list(){
    let move_list=[];
    for( let row=0; row<this._board.get_array().length; row++){
      for( let column=0; column<this._board.get_array()[row].length; column++){
        if(this._board.player_can_play_on(row,column,this._current_color))
          move_list.push(new Move(MoveType.PUT_STONE,this._current_color,row,column,null,null));
      }
    }
    return move_list;
  }

  eat_move_list(){
    let move_list=[];
      for(let eat_bug of this.get_board()._get_enemies_bugs_around(this._eating_bug))
        if(this._eating_bug.can_eat(eat_bug) && this.get_board().can_grow_if_eat_bug(this._eating_bug,eat_bug) )
          move_list.push(new Move(MoveType.EAT,this._current_color,null,null,this._eating_bug,eat_bug));
    return move_list;
  }

  grow_move_list() {

    let move_list = [];
    for (let coordinates of this._board.get_all_coordinates_where_grow_from_bug(this._eating_bug))
      move_list.push(new Move(MoveType.GROW, this._current_color, coordinates[0], coordinates[1],null,null));

    return move_list;
  }

  get_possible_move_list() {
    if (this._phase === Phase.PUT_STONE) {
      return this.put_stone_move_list();
    }
    if (this._phase === Phase.EAT) {
      return this.eat_move_list();
    }
    if (this._phase === Phase.GROW) {
      return this.grow_move_list();
    }
  }

  is_finished() {
    return this._phase === Phase.PUT_STONE && this.get_possible_move_list().length === 0;
  }

  move_is_valid(move) {
    let move_list = this.get_possible_move_list();
    if(this.phase() === Phase.EAT){
      for( let compare_move of move_list){
        if(compare_move.get_type() === move.get_type()
          && compare_move.get_color() === move.get_color()
          && compare_move.get_eating_bug().is_equal_to(move.get_eating_bug())
          && compare_move.get_eated_bug().is_equal_to(move.get_eated_bug()))
          return true;
      }
      return false;
    }
    else{
      for( let compare_move of move_list){
        if(compare_move.get_type() === move.get_type()
          && compare_move.get_color() === move.get_color()
          && compare_move.get_x() === move.get_x()
          && compare_move.get_y() === move.get_y())
          return true;
      }
      return false;
    }

  }


  move(move) {
    if(move._type === MoveType.PUT_STONE) {
      this._board.play_on(move._x,move._y,this.current_color());
      this._eating_bug = this.get_board()._get_bug_on(move._x,move._y);
      for(let eat_bug of this.get_board()._get_enemies_bugs_around(this._eating_bug)) {
        if (this._eating_bug.can_eat(eat_bug) && this.get_board().can_grow_if_eat_bug(this._eating_bug, eat_bug)) {
          this._phase = Phase.EAT;
          return;
        }
      }
      this._change_color();
    }
    else if (move._type === MoveType.EAT) {
      this._board.remove_bug(move.get_eated_bug());
      this._phase = Phase.GROW;
    }
    else if(move._type === MoveType.GROW) {
      this._board.set_stone(move.get_x(), move.get_y(), this._current_color);
      for (let eat_bug of this.get_board()._get_enemies_bugs_around(this._eating_bug)) {
        if (this._eating_bug.can_eat(eat_bug) && this.get_board().can_grow_if_eat_bug(this._eating_bug, eat_bug)) {
          this._phase = Phase.EAT;
          return;
        }
      }
      this._change_color();
      this._phase = Phase.PUT_STONE;
    }
  }


  parse(str) {
    // TODO
  }

  to_string() {
    let string = "Game type : " + this._game_type +
      "\nCurrent color : " + this._current_color +
      "\nCurrent phase : " + this.phase() +
      "\nEating bug : ";
    if (this._eating_bug !== null)
      string += this._eating_bug.toString();
    else
      string += "null";
    string += "\nBoard :\n" + this._board.toString();
    return string;
  }

  winner_is() {
    return this._current_color;
  }

  _change_color(){
    if(this.current_color() === Color.BLACK_STONE) this._current_color = Color.WHITE_STONE;
    else this._current_color = Color.BLACK_STONE;
  }

}

export default Engine;
