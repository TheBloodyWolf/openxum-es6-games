"use strict";

import Color from './color.mjs';
import Board from './board.mjs';

class Bug {
  constructor(x, y, color) {
    this._stones = [];
    this._stones.push([x,y]);
    this._color = color;
  }

// public methods

  clone(){
    let clone_bug = new Bug(this._stones[0][0],this._stones[0][1],this._color);
    for(let bug_index=1;bug_index<this._stones.length;bug_index++){
      clone_bug.add(this._stones[bug_index][0],this._stones[bug_index][1]);
    }
    return clone_bug;
  }

  encode(){
    let str = "";
    for ( let coord_stone of this._stones)
      str += coord_stone[0].toString() + coord_stone[1].toString();
    if(this.get_color() === Color.BLACK_STONE) str += 'B';
    else str += 'W';
    return str;
  }

  decode(str){
    let char = 0;
    let stones = [];
    for (char; char<str.length-2; char+=2)
      stones.push([parseInt(str[char],10),parseInt(str[char+1],10)]);
    this._stones = stones;
    char++;
    if(str[char] === 'B') this._color = Color.BLACK_STONE;
    else this._color = Color.WHITE_STONE;
  }

  is_equal_to(bug){
    if(this._color !== bug.get_color() || this._stones.length !== bug._stones.length)
      return false;
    for(let stone=0;stone<this._stones.length;stone++){
      if(this._stones[stone][0]!==bug._stones[stone][0] || this._stones[stone][1]!==bug._stones[stone][1])
        return false;
    }
    return true;
  }

  get_size(){
    return this._stones.length;
  }

  get_color(){
    return this._color;
  }

  get_stones(){
    return this._stones;
  }

  is_on(x,y){
    for(let stone of this._stones){
      if(stone[0]===x && stone[1]===y)
        return true;
    }
    return false;
  }

  is_opponent_to(bug){
    return this.get_color() === Color.WHITE_STONE && bug.get_color() === Color.BLACK_STONE || this.get_color() === Color.BLACK_STONE && bug.get_color() === Color.WHITE_STONE;
  }

  can_eat(bug){
    if(!this.is_opponent_to(bug))
      return false;
    return this.is_same_shape(bug);
  }

  is_same_shape(bug){
    if(this.get_size() !== bug.get_size()) return false;

    let rotated_bug = bug,mirror_bug = bug.mirror();
    return (this._check_all_rotations(rotated_bug) || this._check_all_rotations(mirror_bug));
  }

  _check_all_rotations(bug){
    let vectors = this.get_vectors(0);
    let compared_vectors;
    for(let rotation = 0; rotation<6; rotation++){
      for(let start_point = 0; start_point < bug.get_size(); start_point++) {
        compared_vectors = bug.get_vectors(start_point);
        if (Bug.compare_vectors(vectors,compared_vectors)) return true;
      }
      bug = bug.rotate();
    }
    return false;
  }

  add(x,y){
    this._stones.push([x,y]);
  }

  mirror(){
    let x_center=this._stones[0][0] ,y_center = this._stones[0][1];
    let mirror_bug = new Bug(x_center*(-1)+x_center*2, y_center,this.get_color());
    let x,y;
    for(let i=1; i<this._stones.length; i++){
      x = this._stones[i][0]*(-1);
      y = this._stones[i][1];
      x = x + (x_center*2);
      y = y -(x-x_center);
      mirror_bug.add(x,y);
    }
    return mirror_bug;
  }

  rotate() {
    let x_center = this._stones[0][0];
    let y_center = this._stones[0][1];
    let rotated_bug = new Bug(x_center,y_center,this.get_color());
    for (let coordinates of this._stones) {
      if(coordinates[0] === rotated_bug._stones[0][0] &&
        coordinates[1] === rotated_bug._stones[0][1]) continue;
      let x = (coordinates[0]-x_center)*Math.cos(-90*Math.PI/180)-(coordinates[1]-y_center)*Math.sin(-90*Math.PI/180);
      let y = (coordinates[0]-x_center)*Math.sin(-90*Math.PI/180)-(coordinates[1]-y_center)*Math.cos(-90*Math.PI/180);
      x += x_center + (coordinates[0] - x_center);
      y += y_center;
      rotated_bug.add(Math.round(x),Math.round(y));
    }
    return rotated_bug;
  }

  toString() {
    if (this.get_size() > 0) {
      let string =  "bug " + (this._color.toString()===Color.BLACK_STONE?"black":"white")  +" on ";
      for (let stone of this._stones) {
        string += "["+stone.toString()+"] ";
      }
      string += "\n";
      return string;
    }
    return '';
  }

  contain(x,y){
    for(let stone of this._stones){
      if(stone === [x,y])
        return true;
    }
    return false;
  }

  get_vectors(indice_start_point){
    let vectors = [];
    if(this._stones[indice_start_point] === undefined) return vectors;
    let x_start = this._stones[indice_start_point][0], y_start = this._stones[indice_start_point][1];
    let x_vector,y_vector;
    for( let i=0 ; i<this._stones.length; i++){
      if(i===indice_start_point) continue;
      x_vector = this._stones[i][0]-x_start;
      y_vector = this._stones[i][1]-y_start;
      vectors.push([x_vector,y_vector]);
    }
    return vectors;
  }

  static compare_vectors(vectors1,vectors2){
    let elem_present;
    for(let elem_vect1 of vectors1) {
      elem_present = false;
      for (let elem_vect2 of vectors2) {
        if (elem_vect1[0] === elem_vect2[0] && elem_vect1[1] === elem_vect2[1])
          elem_present = true;
      }
      if (!elem_present) return false;
    }
    return true;
  }

  get_all_coordinates_around(){
    let coordinates_aroud_bug = [];
    let coordinates_around_point;
    for(let stone of this.get_stones()){
      coordinates_around_point = Board._get_surround_coordinate(stone[0],stone[1]);
      for (let coord of coordinates_around_point){
        if(! this.contain(coord[0], coord[1])
          && Board._check_correct_coordinate(coord[0],coord[1]))
          coordinates_aroud_bug.push(coord);
      }
    }
    return coordinates_aroud_bug;
  }


}

export default Bug;
