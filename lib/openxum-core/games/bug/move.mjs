"use strict";

import MoveType from './move_type.mjs';
import OpenXum from '../../openxum/index.mjs';
import Color from './color.mjs';
import Bug from './bug.mjs';

class Move extends OpenXum.Move {
  constructor(t, c,x,y,eating_bug,eated_bug) {
    super();
    this._type = t;
    this._color = c;
    this._x = x;
    this._y = y;
    this._eating_bug = eating_bug;
    this._eated_bug=eated_bug;
  }

// public methods

  get_type(){
    return this._type;
  }

  get_color(){
    return this._color;
  }

  get_x(){
    return this._x;
  }

  get_y(){
    return this._y;
  }

  get_eating_bug(){
    return this._eating_bug;
  }

  get_eated_bug(){
    return this._eated_bug;
  }

  decode(str) {
    if(str[0]==='E')
      this._decode_move_eat(str);
    else
      this._decode_move_place_stone(str);
  }

  _decode_move_place_stone(str){
    this._type = str[0] === 'S'? MoveType.PUT_STONE: MoveType.GROW;
    this._color = str[1] === 'B'? Color.BLACK_STONE: Color.WHITE_STONE;
    this._x = parseInt(str[2]);
    this._y = parseInt(str[3]);
  }

  _decode_move_eat(str) {
    let index_char=0;
    if(str[index_char] === 'E')
      this._type = MoveType.EAT;
    index_char++;
    this._color = str[index_char] === 'B'? Color.BLACK_STONE: Color.WHITE_STONE;
    let str_bug = "";
    index_char++;
    while(str[index_char] !== 'B' && str[index_char] !== 'W') {
      str_bug += str[index_char];
      index_char++;
    }
    str_bug += str[index_char];
    this._eating_bug = new Bug();
    this._eating_bug.decode(str_bug);
    str_bug = "";
    index_char++;
    while(str[index_char] !== 'B' && str[index_char] !== 'W') {
      str_bug += str[index_char];
      index_char++;
    }
    str_bug += str[index_char];
    this._eated_bug = new Bug();
    this._eated_bug.decode(str_bug);
  }




  encode() {
    if(this._type===MoveType.EAT) {
      return this._encode_move_eat();
    }
    else
      return this._encode_move_place_stone();
  }

  _encode_move_place_stone() {
    return (this._type === MoveType.PUT_STONE? "S": "G")+(this._color === Color.BLACK_STONE? "B": "W")+this._x+this._y;
  }

  _encode_move_eat() {
    let str = "";
    str += 'E';
    if(this._color === Color.BLACK_STONE) str+='B';
    else str+= 'W';
    str += this._eating_bug.encode();
    str += this._eated_bug.encode();
    return str;
  }

  from_object(data) {
    this._type = data.type;
    this._color = data.color;
    this._x = data.x;
    this._y = data.y;
    this._eating_bug = data.eating_bug;
    this._eated_bug = data.eated_bug;
  }

  to_object() {
    return {type:this._type, color:this._color, x:this._x, y:this._y, eating_bug: this._eating_bug, eated_bug: this._eated_bug};
  }

  to_string() {
    if(this._type===MoveType.EAT)
      return this._to_string_move_eat();
    else
      return this._to_string_move_place_stone();
  }

  _to_string_move_place_stone() {
    return "Player "+ (this._color === Color.BLACK_STONE? "Black": "White")+ (this._type === MoveType.PUT_STONE ? " place a stone on " : " grow on ") +"["+ this._x + "," + this._y+']';
  }

  _to_string_move_eat() {
    return "Player "+ (this._color === Color.BLACK_STONE? "Black": "White")+ " eat " + this._eated_bug.toString();
  }

}

export default Move;
